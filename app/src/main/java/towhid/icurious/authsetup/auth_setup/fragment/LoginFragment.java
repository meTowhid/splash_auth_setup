package towhid.icurious.authsetup.auth_setup.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import towhid.icurious.authsetup.MainActivity;
import towhid.icurious.authsetup.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {


    private ViewPager vp;
    private EditText et_name, et_pass;
    private String name, pass;
    private boolean isSuccess;
    private ProgressDialog progressDialog;
    private SharedPreferences pref;
    private View v;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_login, container, false);
        vp = (ViewPager) getActivity().findViewById(R.id.viewPager);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading");

        et_name = (EditText) v.findViewById(R.id.userName);
        et_pass = (EditText) v.findViewById(R.id.password);
        Button btn_login = (Button) v.findViewById(R.id.loginButton);
        Button btn_gotoRegister = (Button) v.findViewById(R.id.gotoRegister);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = et_name.getText().toString().trim();
                pass = et_pass.getText().toString().trim();
                /*if (name.isEmpty() || pass.isEmpty())
                    Toast.makeText(getContext(), R.string.error_none_empty, Toast.LENGTH_SHORT).show();
                else if (pass.length() < 6)
                    Toast.makeText(getActivity(), R.string.error_pass_6_char, Toast.LENGTH_SHORT).show();
                else if (mUtils.isOnline()) {
                    progressDialog.show();
                    String email = mDataProvider.getInstance().checkUserValidity(name);

                    if (email != null && mDataProvider.getInstance().shouldLogin(v))
                        loginUser(email);
                    else {
                        int msg = email == null ? R.string.error_user_doesnt_exists : R.string.unauthorized_login;
                        Snackbar.make(v, msg, Snackbar.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                }*/
            }
        });

        btn_gotoRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(2);
            }
        });
        return v;
    }

    private void loginUser(String email) {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Snackbar.make(v, R.string.auth_failed, Snackbar.LENGTH_LONG).show();
                        } else {
                            startActivity(new Intent(getContext(), MainActivity.class));
                            getActivity().finish();
                        }
                        progressDialog.dismiss();
                    }
                });
    }
}
