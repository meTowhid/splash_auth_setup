package towhid.icurious.authsetup.auth_setup.other;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import towhid.icurious.authsetup.auth_setup.fragment.LoginFragment;
import towhid.icurious.authsetup.auth_setup.fragment.RegisterFragment;
import towhid.icurious.authsetup.auth_setup.fragment.SplashFragment;

/**
 * Created by mac on 2/28/17.
 */

public class AuthAdapter extends FragmentPagerAdapter {

    private ArrayList<Fragment> fragments = new ArrayList<>();

    public AuthAdapter(FragmentManager fm) {
        super(fm);
        fragments.add(new SplashFragment());
        fragments.add(new LoginFragment());
        fragments.add(new RegisterFragment());
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
