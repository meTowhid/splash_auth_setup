package towhid.icurious.authsetup.auth_setup.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import towhid.icurious.authsetup.R;


public class RegisterFragment extends Fragment {

    private ViewPager vp;
    private EditText et_userName, et_email, et_password, et_phone;
    private String userName, email, password, phone;
    private ProgressDialog progressDialog;
    private View v;

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_register, container, false);
        vp = (ViewPager) getActivity().findViewById(R.id.viewPager);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("loading");

        et_userName = (EditText) v.findViewById(R.id.userName);
        et_email = (EditText) v.findViewById(R.id.email);
        et_password = (EditText) v.findViewById(R.id.password);
        et_phone = (EditText) v.findViewById(R.id.phone);
        Button btn_register = (Button) v.findViewById(R.id.registerButton);
        Button btn_back = (Button) v.findViewById(R.id.goBack);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userName = et_userName.getText().toString().trim();
                email = et_email.getText().toString().trim();
                password = et_password.getText().toString().trim();
                phone = et_phone.getText().toString().trim();
                phone = phone.isEmpty() ? null : phone;

                if (userName.isEmpty() || email.isEmpty() || password.isEmpty())
                    Toast.makeText(getContext(), R.string.error_none_empty, Toast.LENGTH_SHORT).show();
                /*else if (mUtils.isOnline()) {
                    registerUser();
                    progressDialog.show();
                }*/
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(1);
            }
        });
        return v;
    }

    private void registerUser() {
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                       /* if (task.isSuccessful()) {
                            String uid = mDataProvider.getInstance().mAuth.getCurrentUser().getUid();
                            User user = new User(userName, email, phone);
                            mDataProvider.getInstance().getRoot().child(uid).setValue(user);
                            mDataProvider.getInstance().mAuth.signOut();

                            et_userName.setText("");
                            et_password.setText("");
                            et_email.setText("");
                            et_phone.setText("");
                        }*/
                        Snackbar.make(v, task.isSuccessful() ? R.string.wait_for_auth
                                : R.string.error_user_exists, Snackbar.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                });
    }
}
