package towhid.icurious.authsetup.auth_setup.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;

import towhid.icurious.authsetup.MainActivity;
import towhid.icurious.authsetup.R;


public class SplashFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (FirebaseAuth.getInstance().getCurrentUser() == null) {
                    ViewPager vp = (ViewPager) getActivity().findViewById(R.id.viewPager);
                    vp.setCurrentItem(1);
                } else {
                    startActivity(new Intent(getActivity(), MainActivity.class));
                    getActivity().finish();
                }
            }
        }, 3000);
        return inflater.inflate(R.layout.fragment_splash, container, false);
    }
}
