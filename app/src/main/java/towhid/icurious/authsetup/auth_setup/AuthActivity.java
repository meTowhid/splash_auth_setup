package towhid.icurious.authsetup.auth_setup;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import towhid.icurious.authsetup.R;
import towhid.icurious.authsetup.auth_setup.other.AuthAdapter;


public class AuthActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(new AuthAdapter(getSupportFragmentManager()));
    }
}
